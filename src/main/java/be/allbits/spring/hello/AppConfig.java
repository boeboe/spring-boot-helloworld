package be.allbits.spring.hello;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.JvmGcMetrics;
import io.micrometer.core.instrument.binder.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.LogbackMetrics;
import io.micrometer.core.instrument.binder.ProcessorMetrics;
import io.micrometer.core.instrument.binder.UptimeMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@Configuration
@PropertySource("classpath:metrics.properties")
public class AppConfig {

    @Autowired
    private MeterRegistry registry;

    @PostConstruct
    public void enableMetrics() {
        new ClassLoaderMetrics().bindTo(registry);
        new JvmMemoryMetrics().bindTo(registry);
        new JvmGcMetrics().bindTo(registry);
        new ProcessorMetrics().bindTo(registry);
        new JvmThreadMetrics().bindTo(registry);
        new LogbackMetrics().bindTo(registry);
        new UptimeMetrics().bindTo(registry);
    }

}
