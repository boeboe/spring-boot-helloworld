.PHONY:	build push build-local

IMAGE = boeboe/spring-boot-micrometer
TAG = 0.1.0

default: build push

build:
	docker build --pull -t ${IMAGE}:${TAG} -f docker/Dockerfile .
	docker tag ${IMAGE}:${TAG} ${IMAGE}:latest

build-clean:
	docker build --pull --no-cache -t ${IMAGE}:${TAG} -f docker/Dockerfile .
	docker tag ${IMAGE}:${TAG} ${IMAGE}:latest

push:
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):latest

build-local:
	docker build --pull -t ${IMAGE}:${TAG} .
	docker tag ${IMAGE}:${TAG} ${IMAGE}:latest